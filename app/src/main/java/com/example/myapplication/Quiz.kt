package com.example.myapplication

val arrayNilai = arrayOf(60, 80, 90, 45, 30)
fun main() {
    val nilaiHuruf = mutableListOf<Char>()
    arrayNilai.forEach {
        nilaiHuruf.add(convertNilai(it))
    }
    //printNilai(nilaiHuruf)
    //Revisi Line
    val nilai: MutableList<Nilai> = mutableListOf()
    arrayNilai.forEach {
        val historyList = arrayListOf(it)
        val objNilai = Nilai(it, false, historyList)
        nilai.add(objNilai)
    }
    nilai[3] = revisiNilai(nilai[3], 80)

    val nilaiHuruf2 = mutableListOf<Char>()
    nilai.forEach {
        nilaiHuruf2.add(convertNilai(it.nilai))
    }
    nilaiHuruf2.forEachIndexed { index, char ->
        printNilai(index, nilai[index], char)
    }
}

fun convertNilai(array: Int): Char {
    val character = when (array.toDouble()) {
        in 80.1..100.0 -> ('A')
        in 60.1..80.0 -> ('B')
        in 50.1..60.0 -> ('C')
        in 40.1..50.0 -> ('D')
        else -> ('E')
    }
    return character
}

fun revisiNilai(nilai: Nilai, newNilai: Int): Nilai {
    var revisi = nilai
    val historyNilai = arrayListOf<Int>()
    historyNilai.addAll(revisi.historyNilai)
    historyNilai.add(newNilai)
    revisi = Nilai(newNilai, true, historyNilai)
    return revisi
}

fun printNilai(nilaiHuruf: MutableList<Char>) {
    nilaiHuruf.forEachIndexed { index, huruf ->
        println("Murid ke-${index + 1} mendapatkan $huruf tepatnya ${arrayNilai[index]}")
    }
}

fun printNilai(index: Int, nilai: Nilai, nilaiHuruf: Char) {
    if (nilai.isRevisi) {
        var string = ""
        nilai.historyNilai.removeLast()
        nilai.historyNilai.forEach {
            string += "yang sebelumnya mendapatkan $it "
        }
        println("Murid ke-${index + 1} mendapatkan $nilaiHuruf tepatnya ${nilai.nilai} $string")
    } else {
        println("Murid ke-${index + 1} mendapatkan $nilaiHuruf tepatnya ${nilai.nilai}")
    }
}

data class Nilai(
    var nilai: Int,
    var isRevisi: Boolean,
    var historyNilai: MutableList<Int>
)